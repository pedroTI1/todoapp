package br.prtt.pdm.todoapp.model

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.IdRes
import br.prtt.pdm.todoapp.R

class TaskList (
    private val context: Context,
    @IdRes private val layoutId: Int,
    private val taskList: ArrayList<Tasks>
) {
    private val container: LinearLayout = (this.context as Activity).findViewById(this.layoutId)
    private val lastListSize: Int = this.taskList.size
    fun build(): TaskList {
        this.render()
        return this
    }

    fun update(forceUpdate: Boolean = false): TaskList {
        if(forceUpdate || lastListSize != this.taskList.size)
        this.render()
        return this
    }



    private fun render() {
        this.container.removeAllViews()
        for(t in taskList ){
            val v: View = View.inflate(this.context, R.layout.card_view, null)
            val  txtTextTask: TextView = v.findViewById(R.id.txtTextTask)
            val imageUrgent: ImageView = v.findViewById(R.id.imageUrgent)
            val checkBoxDone: CheckBox = v.findViewById(R.id.checkBoxDone)
            checkBoxDone.setOnClickListener {
                if(checkBoxDone.isChecked){
                    t.setIsDone(true)
                }else{
                    t.setIsDone(false)
                }
            }
            txtTextTask.text =  t.getDescription()
            if(t.getIsUrgent()){
                imageUrgent.setBackgroundColor(Color.RED)
            }
            checkBoxDone.isChecked = t.getIsDone()
            this.container.addView(v)
        }

    }
}