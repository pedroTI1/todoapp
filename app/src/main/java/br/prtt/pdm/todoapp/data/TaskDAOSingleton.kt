package br.prtt.pdm.todoapp.data

import android.content.Context
import android.util.Log
import br.prtt.pdm.todoapp.model.Tasks

object TaskDAOSingleton {
    private lateinit var tasksList: ArrayList<Tasks>
    private lateinit var dbTaskTable: DBTaskTable
    fun initDAO(context: Context){
        if(!TaskDAOSingleton::tasksList.isInitialized){
            dbTaskTable = DBTaskTable(context)
            tasksList = dbTaskTable.getAllTasks()
        }
    }
    fun add(context: Context, task : Tasks): Int{
        initDAO(context)
        val id = dbTaskTable.insert(task)
        task.setId(id)
        tasksList.add(0, task)
        return 0
    }

    fun update(context: Context, task: Tasks){
        initDAO(context)
        val pos = tasksList.indexOf(task)
        val t =   tasksList[pos]
        t.setIsDone(task.getIsDone())
        dbTaskTable.updateIsDone(t)
    }

    fun delete(context: Context, task: Tasks) : Int {
        initDAO(context)
        val index = tasksList.indexOf(task)
        tasksList.removeAt(index)
        dbTaskTable.delete(task)
        return index
    }

    fun getAllTasks(context: Context):ArrayList<Tasks> {
        initDAO(context)
        Log.d("TEST", "entrou aqui NO SINGLETON")
        return tasksList
    }



}