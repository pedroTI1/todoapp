package br.prtt.pdm.todoapp.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.prtt.pdm.todoapp.R
import br.prtt.pdm.todoapp.model.Tasks

class TaskListAdapter(
        private var tasksList: ArrayList<Tasks>
): RecyclerView.Adapter<TaskViewHolder>() {
    private var backupList = tasksList
    private var listener: OnLongCLickTaskListener? = null
    var lastClickedTask: Tasks? = null

    fun interface OnLongCLickTaskListener{
        fun onClickTask(tasks: Tasks)
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(R.layout.card_view, parent, false)
        return TaskViewHolder(itemView, this)

    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bind(this.tasksList[position])

    }

    override fun getItemCount(): Int {
        return this.tasksList.size
    }

    fun setOnLongClickTaskListener(listener: OnLongCLickTaskListener?): TaskListAdapter {
        this.listener = listener
        return this
    }

    fun getOnLongClickTaskListener(): OnLongCLickTaskListener? {
        return this.listener
    }



    fun filterNotDoneTasks(isNotDone: Boolean = false){
        var aux = ArrayList<Tasks>()
        if(isNotDone){
            for(t in this.tasksList) {
                if (!t.getIsDone()) {
                    aux.add(t)
                }
            }
            this.tasksList = aux
            notifyDataSetChanged()

        }else{
            this.tasksList = this.backupList
            notifyDataSetChanged()
        }
    }



}